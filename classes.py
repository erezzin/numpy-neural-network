import abc
from abc import ABC

import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets
from sklearn.metrics import accuracy_score


class Weights:
    def __init__(self, tensor):
        self.tensor = tensor
        self.gradient = np.zeros_like(self.tensor)


class GDOptimizer:
    def __init__(self, lr=0.01):
        self.lr = lr

    def update(self, weights):
        weights.tensor -= self.lr * weights.gradient
        weights.gradient.fill(0)


class IBackPropagator():
    def back_propagate_and_update(self, dprev):
        pass


class Layer(IBackPropagator):

    def __init__(self):
        self.next_layer = None
        self.prev_layer = None

    @abc.abstractmethod
    def forward_propagate(self, x):
        pass

    @abc.abstractmethod
    def back_propagate_and_update(self, dprev):
        pass

    def set_prev_layer(self, prev_layer: "Layer", update_other=True):
        self.prev_layer = prev_layer
        if update_other:
            prev_layer.set_next_layer(self, False)

    def set_next_layer(self, next_layer: "Layer", update_other=True):
        self.next_layer = next_layer
        if update_other:
            next_layer.set_prev_layer(self, False)


class InnerLayer(Layer):
    def __init__(self, mode_analytical=True):
        super().__init__()
        self._in = None
        self._out = None
        self.mode_analytical = mode_analytical

    @abc.abstractmethod
    def calculate_forward_propagation(self, in_var):
        pass

    @abc.abstractmethod
    def get_analytical_gradient(self, f_in, f_out, dprev):
        pass

    def forward_propagate(self, x):
        self._in = x
        self._out = self.calculate_forward_propagation(x)
        self.next_layer.forward_propagate(self._out)

    @abc.abstractmethod
    def multiply_dprev_and_grad(self, grad, dprev):
        pass

    def back_propagate_and_update(self, dprev):

        if self.mode_analytical:
            grad = self.get_analytical_gradient(self._in, self._out, dprev)
        else:
            grad = self.get_numeric_gradient(self._in)

        back_prop = self.multiply_dprev_and_grad(grad, dprev)

        return self.prev_layer.back_propagate_and_update(back_prop)

    def get_numeric_gradient(self, var):

        if not hasattr(var, '__len__'):
            var = [var]

        ret = np.diag([1e-7] * len(var))

        for i in range(len(var)):
            ret[i] = (self.calculate_forward_propagation(var + ret[i]) - self.calculate_forward_propagation(
                var - ret[i])) / (2 * ret[i])

        return ret

    def __repr__(self):
        return self.__class__.__name__


class SingleActivationLayer(InnerLayer):

    def multiply_dprev_and_grad(self, grad, dprev):
        return grad * dprev


class UpdatableInnerLayer(InnerLayer):
    def __init__(self, mode_analytical=True):
        super().__init__(mode_analytical)
        self.optimizer = None
        self.weights = self.init_weights()

    @abc.abstractmethod
    def init_weights(self):
        pass

    @abc.abstractmethod
    def gradient_by_weights(self, f_in, f_out):
        pass

    def set_optimizer(self, optimizer: GDOptimizer):
        self.optimizer = optimizer

    def back_propagate_and_update(self, dprev):
        grad_by_input = super(UpdatableInnerLayer, self).back_propagate_and_update(dprev)

        self.weights.gradient = self.gradient_by_weights(self._in, self._out).T @ dprev
        self.optimizer.update(self.weights)

        return grad_by_input


class FullyConnectedTopology(UpdatableInnerLayer):
    def __init__(self, input_shape, output_shape):
        self.input_shape = input_shape
        self.output_shape = output_shape
        super(FullyConnectedTopology, self).__init__()

    def multiply_dprev_and_grad(self, grad, dprev):
        return dprev @ grad

    def init_weights(self):
        return Weights(np.random.randn(self.input_shape + 1, self.output_shape))

    def calculate_forward_propagation(self, in_var):
        return in_var @ self.weights.tensor[1:] + self.weights.tensor[0]

    def get_analytical_gradient(self, f_in, f_out, dprev):

        return self.weights.tensor[1:].T

    def gradient_by_weights(self, f_in, f_out):
        return np.concatenate([np.ones((len(f_in),1)), f_in],axis=1)


class SoftMax(InnerLayer):
    def __init__(self):
        super(SoftMax, self).__init__()

    def calculate_forward_propagation(self, in_var):
        exps = np.exp(in_var)
        return exps / np.sum(exps, axis=1, keepdims=True)

    def get_analytical_gradient(self, f_in, f_out, dprev):
        # grad = np.empty((*f_out.shape, f_out.shape[1]))
        # for i in range(f_in.shape[0]):
        #     grad[i] = f_out[i] * np.eye(f_out.shape[1]) - f_out[i] * f_out[[i],:].T
        grad = -f_out[:, None] * f_out[:, :, None]
        m, n = f_out.shape
        grad.reshape(m, n * n)[:, ::n + 1] += f_out
        return grad


class LossFunctional(IBackPropagator):
    def __init__(self):
        super(LossFunctional, self).__init__()

    @abc.abstractmethod
    def calculate_loss_functional(self, pred, true):
        pass

    @abc.abstractmethod
    def get_gradient(self, pred, true):
        pass

    def get_loss(self, pred, true):
        self._pred, self._true = pred, true
        self._out = np.nan_to_num(self.calculate_loss_functional(self._pred, self._true))
        return self._out

    def back_propagate_and_update(self, dprev):
        return self.get_gradient(self._pred, self._true) * dprev


class CrossEntropyLoss(LossFunctional):

    def __init__(self):
        super(CrossEntropyLoss, self).__init__()

    def calculate_loss_functional(self, pred, true):
        return -np.mean(true * np.log(pred))

    def get_gradient(self, pred, true):
        return true / pred / len(true)


class SoftMaxCrossEntropy(LossFunctional):

    def __init__(self):
        super(SoftMaxCrossEntropy, self).__init__()

    def calculate_loss_functional(self, pred, true):
        exps = np.exp(pred)
        exps /= np.sum(exps, axis=1, keepdims=True)
        return -np.mean(true * np.log(exps))

    def get_gradient(self, pred, true):
        exps = np.exp(pred)
        exps /= np.sum(exps, axis=1, keepdims=True)
        return (exps - true) / len(true)


class LinearLayer(SingleActivationLayer):

    def calculate_forward_propagation(self, x):
        return x

    def get_analytical_gradient(self, f_in, f_out, dprev):
        return 1


class ReLU(SingleActivationLayer):
    def __init__(self):
        super(ReLU, self).__init__()

    def calculate_forward_propagation(self, x):
        return np.maximum(0, x)

    def get_analytical_gradient(self, f_in, f_out, dprev):
        return np.maximum(0, f_in > 0)


class Tanh(SingleActivationLayer):
    def __init__(self):
        super(Tanh, self).__init__()

    def calculate_forward_propagation(self, x):
        exp = np.exp(2 * x)
        return (exp - 1) / (exp + 1)

    def get_analytical_gradient(self, f_in, f_out, dprev):
        return 1 - f_out ** 2


class Sigmoid(SingleActivationLayer):
    def __init__(self):
        super(Sigmoid, self).__init__()

    def calculate_forward_propagation(self, x):
        return 1 / (1. + np.exp(-x))

    def get_analytical_gradient(self, f_in, f_out, dprev):
        return f_out * (1 - f_out)


class NetworkGraph(Layer):
    def __init__(self, input_size, layers: "Iterable[Tuple[InnerLayer, int]]", loss_function: LossFunctional,
                 optimizer: GDOptimizer):

        super().__init__()

        self.optimizer = optimizer
        prev = self
        prev_size = input_size
        for layer, size in layers:
            connection = FullyConnectedTopology(prev_size, size)
            connection.set_prev_layer(prev)
            connection.set_optimizer(optimizer)
            layer.set_prev_layer(connection)
            prev = layer
            prev_size = size
            prev.set_prev_layer(connection)

        layer.set_next_layer(self)

        self.propagating = False

        self.last_layer = layers[-1]

        self.loss_function = loss_function

    def forward_propagate(self, inputs):
        self.propagating = ~self.propagating
        if self.propagating:
            self.next_layer.forward_propagate(inputs)
            return self._pred
        else:
            self._pred = inputs

    def back_propagate_and_update(self, dprev = None):
        self.propagating = ~self.propagating
        if self.propagating:
            dprev = self.loss_function.back_propagate_and_update(1)
            self.prev_layer.back_propagate_and_update(dprev)
        else:
            return

    def get_loss(self, X, y):
        self.predict(X)
        OH_y = np.zeros_like(self._pred)
        OH_y[np.arange(OH_y.shape[0]), y] = 1
        return self.loss_function.get_loss(self._pred, OH_y)

    def fit(self, X, y):
        self.get_loss(X, y)
        self.back_propagate_and_update()

    def predict(self, X, y=None):
        return np.argmax(self.forward_propagate(X), axis=1)

    def fit_predict(self, X, y):
        self.fit(X, y)
        return np.argmax(self._pred, axis=1)


if __name__ == "__main__":

    # from sklearn.neural_network import MLPClassifier

    def plot_decision_boundary(X, y, pred_func):
        # Set min and max values and give it some padding
        x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
        y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
        h = 0.01

        # Generate a grid of points with distance h between them
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

        # Predict the function value for the whole gid
        Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)

        # Plot the contour and training examples
        plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
        plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
        plt.show()


    def make_classification_dataset(num_samples=1024, noise=0.1, display=True):
        # toy dataset
        X, y = datasets.make_moons(num_samples, noise=noise)

        if display:
            # visualize dataset
            plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
            plt.show()
        # X = np.concatenate([X[:,[0]], X], axis=1)
        return X, y


    X, y = make_classification_dataset()
    # nn = MLPClassifier()
    nn = NetworkGraph(2,
                      ((Tanh(), 40),
                       (Tanh(), 40),
                       (LinearLayer(), 2)),
                      SoftMaxCrossEntropy(),
                      GDOptimizer()
                      )
    # nn.fit(X, y)
    preds = nn.predict(X, y)
    print(accuracy_score(y, preds))

    # predict on training set
    for i in range(50):
        preds = nn.fit_predict(X, y)
        print(accuracy_score(y, preds))

    # visualize decision boundary
    plot_decision_boundary(X, y, lambda x: nn.predict(x))
