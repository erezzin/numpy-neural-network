from __future__ import absolute_import, division, print_function, unicode_literals

import datetime
import numbers
import os
from types import GeneratorType
from typing import Iterable, Tuple, Union, Collection, Generator

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn import datasets
from tensorflow import keras

tf.enable_eager_execution()


class SimpleCategoricalFCModel:

    """
    This class represents a simple (uniform layer behavior, expects 1d data-array) fully-connected (dense)
    TensorFlow graph using the Keras API.
    """

    def __init__(
            self, input_size: int,
            layers: Collection[Tuple[str, int]],
            initializer: Union[str, keras.initializers.Initializer] = "glorot_normal",
            optimizer: Union[str, keras.optimizers.Optimizer] = "adam",
            loss: Union[str, keras.losses.Loss] = "categorical_crossentropy",
            dropout: Union[int, keras.layers.Dropout] = 0,
            metrics: Union[Collection[Union[str, keras.metrics.Metric]], None] = None,
            use_bias: bool = True,
            tensorboard_dir: Union[None, str, keras.callbacks.TensorBoard] = "logs/scalars/",
            checkpoint_path: Union[None, str, keras.callbacks.ModelCheckpoint] = "logs/chkpnt.fhd5",
            load_from_checkpoint: bool = False,
            callbacks: Union[None, Iterable[keras.callbacks.Callback]] = None,
            verbose: int = 1,
    ):
        """
        Initializes the wrapper class.

        :param input_size: REQUIRED: The 1d expected size of flattened data.
        :param layers: REQUIRED: The fully-connected model's layer specification as tuples of the activation name
        followed by the layer's width.
        :param initializer: A weight initializer method name or instance. DEFAULT = "glorot_normal" (Gaussian Xavier)
        :param optimizer: A weight optimizer name or instance. DEFAULT = "adam"
        :param loss: A loss function name or instance. DEFAULT = "categorial_crossentropy"
        :param dropout: The dropout-method specification. If a number is passed, than it's used to specify the dropped ratio.
        Also, a custom 'Dropout'-derived class instance can be passed. DEFAULT = 0
        :param metrics: A list or a tuple of learning-metrics to use. The metrics can be passed as metric names (stings)
        or as instances of classes deriving from 'tf.keras.metric.Metric' class. If 'None' is passed, the metric
        'categorical_accuracy' is used. DEFAULT = None
        :param use_bias: A boolean indicating whether to add a bias weight in the fully-conencted layers. DEFAULT = True
        :param load_from_checkpoint: A boolean that signals the class whether to to load from an older
        checkpoint if one exists, or start from scratch. DEFAULT = False
        :param checkpoint_path: If 'None', don't load/save checkpoints. If string, loads a serialized weight-state from
        the given path if exists, and save concurrent checkpoints to that path during runs. If a custom
        'tf.keras.callbacks.ModelCheckpoint' is given, uses it to save the checkpoint, and attempts to load previous
        checkpoint from the path that this callback will write its checkpoints to. If 'load_from_checkpoint' is set
        to 'False', no loading of previous checkpoints occurs. DEFAULT = "logs/chkpnt.fhd5"
        :param tensorboard_dir: If 'None', don't export to TensorBoard. If string, create a TensorBoard callback that
        outputs to the path specified. Also, a custom TensorBoard callback may be passed to be used instead.
        DEFAULT = "logs/scalars"
        :param callbacks: A list of custom callbacks (deriving from 'tf.keras.callbacks.Callback') to be used during the
        training. DEFAULT = None.
        :param verbose: The verbosity of the operation of the graph. 0 - quiet, 1 - prints messages. DEFAULT: 1
        """
        # assign parameters
        self._initializer = initializer
        self.callbacks = callbacks
        self.verbose = verbose
        self._tensorboard_dir = tensorboard_dir
        self._dropout = dropout
        self._metrics = metrics
        self._loss = loss
        self._optimizer = optimizer
        self._input_size = input_size
        self._checkpoint_path = checkpoint_path
        self.load_from_checkpoint = load_from_checkpoint

        self._process_parameters_and_ensure_types()

        prev_layer = input = keras.Input(self._input_size, name="input_layer")

        if len(layers) == 0:
            raise ValueError("Number of layers can not be 0.")

        for activation, size in layers:
            prev_layer = tf.keras.layers.Dense(size,
                                               activation=activation,
                                               use_bias=use_bias,
                                               kernel_initializer=initializer)(prev_layer)
            if self._dropout is not None:
                prev_layer = self._dropout(prev_layer)

        # Wrap in a Model
        self.model = tf.keras.Model(inputs=input, outputs=prev_layer)

        if self._checkpoint_path is not None and os.path.isfile(self._checkpoint_path.filepath) \
                and self.load_from_checkpoint:
            self.model.load_weights(self._checkpoint_path.filepath)

        self.model.compile(optimizer=self._optimizer,
                           loss=self._loss,
                           metrics=self._metrics)

    def print_summary(self):
        """
        Prints a string summary of the underlying network-model.
        :return: None
        """
        self.model.summary()

    def _process_parameters_and_ensure_types(self):
        """
        A method that takes arguments passed to the __init__ function and which are allowed multiple formats/types
        and processes them such that their types are eventually uniform.
        :return:
        """
        if isinstance(self._dropout, numbers.Number):
            self._dropout = keras.layers.Dropout(self._dropout)

        if self._metrics is None:
            self._metrics = [keras.metrics.categorical_accuracy]

        if isinstance(self._tensorboard_dir, str):
            tb_logs_dir = self._tensorboard_dir + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
            self._tensorboard_dir = keras.callbacks.TensorBoard(tb_logs_dir)

        if isinstance(self._checkpoint_path, str):
            self._checkpoint_path = keras.callbacks.ModelCheckpoint(self._checkpoint_path)

        if self.callbacks is None:
            self.callbacks = []

    def fit(self, train_data: Union[Tuple[Iterable, Iterable], Generator],
            validation_data: [None, Tuple[Iterable, Iterable]] = None,
            batch_size: int = 256,
            steps_n: int = 10,
            epochs: int = 20,
            shuffle: bool = True,
            use_multiprocessing: bool = False
            ):

        """
        Fits the model on the data provided by the "train_data". This function has two modes - a 'data-set' mode and a
        'data-generator' mode. The 'data-generator' mode is assumed when a 'generator' type is passed in the 'train_data'
        variables, and the 'data-set' mode is assumed when a data-set type is passed as the argument. A data-set is
        a 2-tuple of the training data X and its corresponding labels y (i.e. 'train_data = (X,y)').
        :param train_data: REQUIRED. The data to train on - Either a generator or a data-table. Read the function's docs
        for more details.
        :param validation_data: The data to validate the training on. When in 'generator' mode, accepts either a generator
        and data-sets. When in 'data-set' mode, this method accepts only data-sets for this argument. DEFAULT = None
        :param epochs: An integer representing number of epochs (iterations over the data) for training. DEFAULT = 20
        :param batch_size: Only relevant in 'data-set' mode. An integer which specifies the number of rows in the data
        to take in every batch. DEFAULT = 256
        :param steps_n: Only relevant in 'generator' mode. An integer which specifies the number of steps in each epoch.
        DEFAULT = 10
        :param shuffle: A boolean that specifies whether the batch's data should be shuffled before processing.
        DEFAULT = True
        :param use_multiprocessing: Only relevant in 'generator' mode. A boolean which specifies whether to use more
        that a single processor during training. DEFAULT = False
        :return: None
        """

        # arguments common to both modes
        common_args = {"validation_data": validation_data, "shuffle": shuffle,
                       "use_multiprocessing": use_multiprocessing, "epochs": epochs, "callbacks": self.callbacks,
                       "verbose":self.verbose}

        if self._tensorboard_dir is not None:
            common_args["callbacks"].append(self._tensorboard_dir)

        if self._checkpoint_path is not None:
            common_args["callbacks"].append(self._checkpoint_path)

        if isinstance(train_data, GeneratorType):
            self.model.fit_generator(generator=train_data, steps_per_epoch=steps_n,
                                     validation_steps=steps_n,
                                     **common_args)
        else:
            self.model.fit(*train_data, batch_size=batch_size, **common_args)

    def predict(self, test_data: Union[Tuple[Iterable, Iterable], Generator],
                batch_size: int = 256,
                use_multiprocessing: bool = False):

        """
        Predicts the labels of the data 'test_data' based on any previous fitting on the. This function has two modes -
        a 'data-set' mode and a 'data-generator' mode. The 'data-generator' mode is assumed when a 'generator' type is
        passed in the 'test_data' variables, and the 'data-set' mode is assumed when a data-set type is passed as the
        argument. A data-set is a 2-tuple of the training data X and its corresponding labels y (i.e. 'train_data
        = (X,y)').
        :param test_data: REQUIRED. The data whose labels are sought - Either a generator or a data-table. Read the
        function's docs for more details.
        :param batch_size: Only relevant in 'data-set' mode. An integer which specifies the number of rows in the data
        to take in every batch. DEFAULT = 256
        :param use_multiprocessing: Only relevant in 'generator' mode. A boolean which specifies whether to use more
        that a single processor during training. DEFAULT = False
        :return: An array of labels associated with the 'test_data'
        """

        common_args = {"use_multiprocessing": use_multiprocessing, "verbose": self.verbose}

        if isinstance(test_data, GeneratorType):
            return self.model.predict_generator(generator=test_data, **common_args)
        else:
            return self.model.predict(test_data, batch_size=batch_size, **common_args)


if __name__ == "__main__":

    # region Danny's provided visualization & data generation functions

    def plot_decision_boundary(X, y, pred_func):
        # Set min and max values and give it some padding
        x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
        y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
        h = 0.01

        # Generate a grid of points with distance h between them
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

        # Predict the function value for the whole gid
        Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)

        # Plot the contour and training examples
        plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
        plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
        plt.show()


    def make_classification_dataset(num_samples=1024, noise=0.1, display=False):
        # toy dataset
        X, y = datasets.make_moons(num_samples, noise=noise)

        if display:
            # visualize dataset
            plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
            plt.show()

        y = np.concatenate([[y], [1 - y]]).T

        return X, y


    def classification_dataset_generator(batch_size=1024, noise=0.1):
        while True:
            yield make_classification_dataset(batch_size, noise, False)

    # endregion

    simple_model = SimpleCategoricalFCModel(2, [("relu", 64), ("relu", 128),
                                                ("relu", 64), ("relu", 128),
                                                ("relu", 64), ("softmax", 2)],
                                            loss=keras.losses.categorical_crossentropy)

    simple_model.print_summary()

    simple_model.fit(train_data=classification_dataset_generator(10000),
                     validation_data=make_classification_dataset(10000), epochs=20)

    # visualize decision boundary
    X, y = make_classification_dataset(10000, noise=.2)

    plot_decision_boundary(X, np.argmax(y, axis=1), lambda x: simple_model.predict(x)[:,1])
